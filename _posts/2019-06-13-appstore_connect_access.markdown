---
title: AppStoreConnect 역할별 접근 권한
layout: post
date: '2019-06-13'
tags:
- apple 
- iOS
comments: true
categories: iOS
---

iOS 내부 테스트를 위해서는 AppStore Connect 에 사용자의 애플 계정이 등록이 선행되어야 한다.
사용자 계정의 계정을 등록시 다음과 같은 역할 중 하나를 부여해야한다.

![]({{ site.url }}/_resource/appstoreConnect/appstoreConnect_role_list.png)

